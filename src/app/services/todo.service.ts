import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Todo } from '../interfaces/todo';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  todoTitle: string = '';
  todos: Todo[] = [];
  beforeEdit: string = '';

  constructor(private httpClient: HttpClient) {
   }

   getTodos() {
    return this.httpClient.get(API_URL);
  }

  addTodo(todoTitle: string): void {
    console.log("In add");
    
    if (todoTitle.trim().length === 0) {
      return;
    }
    this.httpClient.post(API_URL, {
      todo: todoTitle,
      completed: false,
    })
      .subscribe((response: any) => {
        this.todos.push({
          id: response.id,
          todo: todoTitle,
          completed: false,
          editing: false
        });
      });

  }

  editTodo(todo: Todo): void {
    this.beforeEdit = todo.todo;
    todo.editing = true;
  }

  doneEdit(todo: Todo): void {
    this.httpClient.put(API_URL + "/" + todo.id, {
      todo: todo.todo,
      completed: todo.completed
    })
      .subscribe((response: any) => {
      });
      // alert('Todo Edited Successfully')
  }

  deleteTodo(id: number): void {
    this.httpClient.delete(API_URL + "/" + id)
    .subscribe((response: any) => {
        this.todos = this.todos.filter(todo => todo.id !== id);
      })
      alert('Todo Deleted Successfully');
  }


  
}
