import { trigger, transition, style, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/interfaces/todo';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  animations: [
    trigger('fade', [

      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(30px)' }),
        animate(200, style({ opacity: 1, transform: 'translateY(0px)'}))
      ]),

      transition(':leave', [
        animate(200, style({ opacity: 0, transform: 'translateY(30px)' }))
      ]),

    ])
  ]
})
export class TodoListComponent implements OnInit {

  todoTitle:string;
  todoData: any;
  constructor(public todoService: TodoService) { }

  ngOnInit(): void {
    this.getTodos();
    this.todoTitle = '';
  }

  getTodos(){
    this.todoService.getTodos().subscribe(data => {
      console.log(data);
      this.todoData = data;
      // console.log("TODO ARRAY",this.todoData);
      
  });
  }

  addTodo(): void {
    if (this.todoTitle.trim().length === 0) {
      return;
    }

    this.todoService.addTodo(this.todoTitle);
    this.todoTitle = '';

  console.log(this.todoTitle);
  
  }

}
