import { Component, Input, OnInit } from '@angular/core';
import { Todo } from 'src/app/interfaces/todo';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-modify-todo',
  templateUrl: './modify-todo.component.html',
  styleUrls: ['./modify-todo.component.scss']
})
export class ModifyTodoComponent implements OnInit {

  @Input() todo: Todo;

  constructor(public todoService: TodoService) { 
  }

  ngOnInit(): void {
  }

}
